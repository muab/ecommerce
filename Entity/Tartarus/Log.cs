﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tartarus
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class Log
    {
        public Log()
        {
        }
        
        public long Id { get; set; }
        public string Logger { get; set; }
        public string Message { get; set; }
        public string Level { get; set; }
        public string Thread { get; set; }
        public string Exception { get; set; }
        public DateTime Tarih { get; set; }

    }
}
