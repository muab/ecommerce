﻿using Microsoft.Data.Entity;

namespace Tartarus.DataAccessLayer.Contexts
{
    public class TartarusContext : DbContext
    {
        public TartarusContext()
        {
        }

        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Log>().HasKey(v => v.Id);

            base.OnModelCreating(builder);
        }

    }
}
