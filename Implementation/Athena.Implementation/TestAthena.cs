﻿using Tartarus;
using Tartarus.DataAccessLayer.Repositories;

namespace Athena.Implementation
{
    public interface ITestAthena
    {
        bool Insert(Log item);
    }
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class TestAthena : ITestAthena
    {
        public TestAthena()
        {
        }

        public bool Insert(Log item)
        {
            return new LogRepository().Insert(item);
        }
    }
}
