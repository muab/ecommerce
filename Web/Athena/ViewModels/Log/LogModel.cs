﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Athena.ViewModels.Log
{
    public class LogModel
    {
        public string Content { get; set; }
        public string Level { get; set; }
        public string Logger { get; set; }
    }
}
