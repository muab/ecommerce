﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Athena.Implementation;
using Athena.ViewModels.Log;
using Tartarus.DataAccessLayer.Contexts;

namespace Athena.Controllers
{
    public class HomeController : Controller
    {
        private ITestAthena TestAthena { get; set; }
        public HomeController()
        {
            TestAthena = new TestAthena();
        }

        public HomeController(ITestAthena testAthena)
        {
            TestAthena = testAthena;
        }

        public IActionResult Insert(LogModel item)
        {
            //TestAthena.Insert(new Tartarus.Log {
            //    Exception = null,
            //    Level = item.Level,
            //    Logger = item.Logger,
            //    Message = item.Content,
            //    Tarih = DateTime.Now,
            //    Thread = null                       
            //});
            var dbContext = new TartarusContext();
            dbContext.Logs.Add(new Tartarus.Log
            {
                Exception = null,
                Level = item.Level,
                Logger = item.Logger,
                Message = item.Content,
                Tarih = DateTime.Now,
                Thread = null
            });
            dbContext.SaveChanges();
            return View();
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
